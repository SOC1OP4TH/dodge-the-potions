# Dodge the Potions
This is a 2D single-player or multiplayer game where you must dodge three potions that fall from the sky. The red potion will damage you, the blue potion will increase your speed, and the green potion will give you an extra life.

## How to Play

To play the game, simply move your character around the screen using the A and D keys. You must dodge the potions that fall from the sky. If you touch a red potion, you will lose a life. If you touch a blue potion, you will move faster. If you touch a green potion, you will gain an extra life.


## Multiplayer Feature

Dodge the Potions can be played in single-player or multiplayer mode. In multiplayer mode, two players can play the game at the same time.

When the game is played in multiplayer mode, players share the same screen. Potions fall at the same time for both players. Players try to dodge potions without blocking each other. 
Currently, the multiplayer gameplay in Dodge the Potions is experiencing some bugs.

## Winning the Game

The goal of the game is to survive for as long as possible. The player with the most lives at the end of the game wins.

## Tips

* Be aware of your surroundings. The potions can come from anywhere.
* Don't be afraid to experiment. There are many different ways to play the game. Find a strategy that works for you.
### Have fun!

I hope you enjoy playing Dodge the Potions!
