using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine.SceneManagement;
using UnityEngine;
//using System.Net.NetworkInformation;

public class OnlineLobby : MonoBehaviourPunCallbacks

{
    // Start is called before the first frame update
     System.Net.NetworkInformation.Ping ping = new System.Net.NetworkInformation.Ping();

    void Start()
    {
        PhotonNetwork.ConnectUsingSettings();
      
        

    }
    public override void OnConnectedToMaster()
    {
        PhotonNetwork.JoinLobby();
    }
  
    public override void OnJoinedLobby()
    {
   
        SceneManager.LoadScene("OnlineLobby");
     
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
