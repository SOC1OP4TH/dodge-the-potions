using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Photon.Pun;
public class Board : MonoBehaviour
{
    // Start is called before the first frame update
    int score;
    
    
    TMP_Text scoreText;
    TMP_Text livesText;
    void Start()
    {
      GameObject scoreBoard = GameObject.FindGameObjectWithTag("ScoreBoard");
        scoreText = scoreBoard.GetComponent<TMP_Text>();
        GameObject livesBoard = GameObject.FindGameObjectWithTag("Lives");
        livesText = livesBoard.GetComponent<TMP_Text>();
    }
    void Update(){

    }

   public void IncreaseScore(int amount){
        score+=amount;
        scoreText.text="Score: " + score.ToString();
    }
    
   public void LivesInfo(int lives){
    
        livesText.text="Lives: " + lives.ToString();
    }
}
