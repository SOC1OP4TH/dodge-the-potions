using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField]
    public GameObject poison;
    public GameObject speed;
    public GameObject heal;


    private float poisonTimer = 3f;
    private float speedTimer = 0f;
    private float healTimer = 0f;
    public GameObject parentGameObject;




    // Start is called before the first frame update
    void Start()
    {
        parentGameObject = GameObject.FindWithTag("Clone");

    }


    // Update is called once per frame
    void Update()
    {
        PoisonSpawnTime();
        SpeedSpawnTime();
        HealSpawnTime();

    }
    
    void PoisonSpawnTime()
    {
        float poisonSpawnSpeed = Random.Range(1, 6);
        if (poisonTimer < poisonSpawnSpeed)
        {
            poisonTimer += Time.deltaTime;
        }
        else
        {

            PoisonSpawner();
            poisonTimer = 0;
        }
    }
    void SpeedSpawnTime()
    {
        float speedSpawnSpeed = Random.Range(5, 9);
        if (speedTimer < speedSpawnSpeed)
        {
            speedTimer += Time.deltaTime;
        }
        else
        {

            SpawnSpeed();
            speedTimer = 0;
        }
    }
    void HealSpawnTime()
    {
        float healSpawnSpeed = Random.Range(4, 9);
        if (healTimer < healSpawnSpeed)
        {
            healTimer += Time.deltaTime;
        }
        else
        {

            SpawnHeal();
            healTimer = 0;
        }

    }
    void PoisonSpawner()
    {
        //spawn knives 
        float random = Random.Range(-4, 4);

        poisonTimer += Time.deltaTime;
        GameObject obj = Instantiate(poison);

        // set the position of the object
     obj.transform.position = new Vector3(random, 5,0);
        obj.transform.parent = parentGameObject.transform;





    }

    void SpawnSpeed()
    {
        float random = Random.Range(-4, 4);
        speedTimer += Time.deltaTime;
        GameObject obj = Instantiate(speed);
        obj.transform.position = new Vector3(random, 5,0);
        obj.transform.parent = parentGameObject.transform;
    }
    void SpawnHeal()
    {
        float random = Random.Range(-4, 9);
        healTimer += Time.deltaTime;
        GameObject obj = Instantiate(heal);
            obj.transform.position = new Vector3(random, 5,0);
        obj.transform.parent = parentGameObject.transform;

    }

}
