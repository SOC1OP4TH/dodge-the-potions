using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;


public class PlayerControls : MonoBehaviour
{
    [SerializeField]
    public Animator animator;
    [SerializeField]
    public float speed = 5f;
    PhotonView view;
    Rigidbody2D rb;
    private float direction = 0.5f;
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        view = GetComponent<PhotonView>();
    }

    // Update is called once per frame
    void Update()
    {
        if(view.IsMine)
        Move();
    }
    void Move()
    {
        Vector3 moveVelocity = Vector3.zero;
        animator.SetBool("isRun", false);


        if (Input.GetAxisRaw("Horizontal") < 0)
        {
            direction = -0.5f;
            moveVelocity = Vector3.left;

            transform.localScale = new Vector3(direction, 0.5f, 0.5f);

            animator.SetBool("isRun", true);

        }

        if (Input.GetAxisRaw("Horizontal") > 0)
        {
            direction = 0.5f;
            moveVelocity = Vector3.right;

            transform.localScale = new Vector3(direction, 0.5f, 0.5f);

            animator.SetBool("isRun", true);

        }
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            animator.SetTrigger("attack");
        }
        transform.position += moveVelocity * speed * Time.deltaTime;

    }


    void ChangeSpeed(float newspeed)
    {
        speed = newspeed;
    }

}
//todos stop spawner
//todos stop player
//ui