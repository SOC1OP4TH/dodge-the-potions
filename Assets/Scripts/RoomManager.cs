using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using TMPro;
using UnityEngine.SceneManagement;


public class RoomManager : MonoBehaviourPunCallbacks
{
    [SerializeField]
    private TMP_InputField join;
    [SerializeField]
    private TMP_InputField create;
    // Start is called before the first frame update
    public void CreateRoom()
    {
       
        if(create.text!="")
        {
            Debug.Log("Created");
            PhotonNetwork.CreateRoom(create.text);
        }
      
    }
    public void JoinRoom()
    {
       
        if(join.text!="")
        {
            PhotonNetwork.JoinRoom(join.text);
        }
      
    }
    public override void OnJoinedRoom()
    {
     
          PhotonNetwork.LoadLevel("Game");
      
    }
    public void Back()
    {
        SceneManager.LoadScene("Lobby");
        PhotonNetwork.Disconnect();
    }
    


}
