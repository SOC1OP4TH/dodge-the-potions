using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Incendia : MonoBehaviour
{
    [SerializeField] private GameObject laser;
    Animator animator;
    private Camera cam;
    Ray ray ;
    private ParticleSystem laserParticleSystem;


    void Start()
    {
        cam = Camera.main;
        laserParticleSystem = laser.GetComponent<ParticleSystem>();
        animator = GetComponent<Animator>();
    }

    void Update()
    {
        Vector3 mousePos = Input.mousePosition;
        mousePos = cam.ScreenToWorldPoint(mousePos);
        //get name with ray
    
    
        //laser.transform.position = transform.position;
        laser.transform.rotation = Quaternion.FromToRotation(Vector3.down, mousePos - transform.position);
        Debug.DrawRay(transform.position, mousePos - transform.position, Color.blue);
        ProcessFiring();
    }

    void ProcessFiring()
    {
        if (Input.GetButton("Fire1"))
        {
            animator.SetTrigger("attack");
            SetLaserActive(true);

        }
        else
        {
            SetLaserActive(false);
        }
    }

    void SetLaserActive(bool isActive)
    {
        var emissionModule = laserParticleSystem.emission;
        emissionModule.enabled = isActive;
    }
}
