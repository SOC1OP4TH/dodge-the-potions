using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class SpawnPlayers : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject playerPrefab;
    float minX;
    float maxX;
    float minY;
    float maxY;
   

    
    void Start()
    {
        Vector2 randompos = new Vector2(Random.Range(minX,maxX),Random.Range(minY,maxY));
        PhotonNetwork.Instantiate(playerPrefab.name,randompos,Quaternion.identity);
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
