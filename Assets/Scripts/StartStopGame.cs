using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class StartStopGame : MonoBehaviour
{

    
    void Start(){
        CheckInternetConnection();
    }
    public void StartTheGame(){
        SceneManager.LoadScene("Game");
    }
    public void ExitTheGame(){
        Application.Quit();
    }
    //  public bool CheckInternetConnection(){
        
    //     Ping ping = new Ping("1.1.1.1");
    //     bool isConnected = ping.isDone;
    //      Debug.Log(isConnected);
    //      if(isConnected)
    //      return true;
    //         else return false;
    // }
    public bool  CheckInternetConnection()
    {
            NetworkReachability reachability = Application.internetReachability;

            if (reachability == NetworkReachability.NotReachable)
            {
               return false;
            }
            else
            {
                return true;
            }
        
    }
    public void OnlineLobby(){
            if(CheckInternetConnection())
                SceneManager.LoadScene("Loading");
            else
                Debug.Log("Internet Yoxdu");
        
    }
    public void RestartOfflineGame(){
        
        SceneManager.LoadScene("GameOffline");
    }
    public void RestartOnlineGame(){
        SceneManager.LoadScene("Game");
    }
    public void MainMenu(){
        SceneManager.LoadScene("Lobby");
    }

}
