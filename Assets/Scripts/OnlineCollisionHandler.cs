using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
public class OnlineCollisionHandler : MonoBehaviour
{
     public int heal = 5;
    public Animator animator;
  private Canvas Canvas;

    Board infoBoard;

    PlayerControls controls;
    //GameObject spawner ;

    Rigidbody2D rb;

    // Start is called before the first frame update
    void Start()
    {
        Canvas = FindObjectOfType<Canvas>();
        Canvas.enabled = false;
        animator = GetComponent<Animator>();
        if (animator == null)
        {
            Debug.LogError("Animator not found!");
        }
        rb = GetComponent<Rigidbody2D>();
         infoBoard = FindObjectOfType<Board>();
        if (infoBoard == null)
        {
            Debug.LogError("Board not found!");
        }

    }

    // Update is called once per frame
    void Update()
    {
        scorer();
        healControl();
    }
    

    // Update is called once per frame

    void OnCollisionEnter2D(Collision2D other)
    {

        if (other.gameObject.tag == "Poison")
        {

            animator.SetTrigger("hurt");
            Destroy(other.gameObject);
            heal--;

        }
        else if (other.gameObject.tag == "Heal")
        {
            heal++;
            Destroy(other.gameObject);
        }
        else if (other.gameObject.tag == "Speed")
        {
            StartCoroutine(SpeedUp());
            Destroy(other.gameObject);
        }

        if (heal == 0)
        {
            animator.SetTrigger("die");
            Canvas.enabled = true;
        
            GetComponent<PlayerControls>().enabled = false;
            PhotonNetwork.Destroy(gameObject);
            ShowCanvasAfterDelay(0.1f);
            
        }
    }
    
    IEnumerator ShowCanvasAfterDelay(float delay)
    {
        yield return new WaitForSeconds(delay);
        Canvas.enabled = true;

        // Disable any spawners that may still be active
        Spawner spawner = FindObjectOfType<Spawner>();
        if (spawner != null)
            spawner.enabled = false;
    }
    IEnumerator SpeedUp()
    {
        float originalSpeed = GetComponent<PlayerControls>().speed;
        GetComponent<PlayerControls>().speed = 20f; // set the speed to 20
        yield return new WaitForSeconds(3f); // wait for 3 seconds
        GetComponent<PlayerControls>().speed = originalSpeed; // restore the original speed  

    }
    void scorer()
    {
        switch (heal)
        {
            case 5:
                infoBoard.IncreaseScore(1);
                break;
            case 4:
                infoBoard.IncreaseScore(2);
                break;
            case 3:
                infoBoard.IncreaseScore(3);
                break;
            case 2:
                infoBoard.IncreaseScore(4);
                break;
            case 1:
                infoBoard.IncreaseScore(5);
                break;
            default:
                infoBoard.IncreaseScore(1);
                break;
        }
    }

    void healControl()
    {
        infoBoard.LivesInfo(heal);
    }
}
